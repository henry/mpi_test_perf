#!/bin/sh


mkdir build
cd build
rm -rf *

if [[ "$(hostname)" == login0*cluster ]]; then
  echo "reconstruit le binaire $0 au mesocentre"
  if [[ -z "${MPI_RUN}" ]]; then
          echo "charger les modules et relancer ce script"
          echo " "
          echo "module load userspace"
          echo "module load cmake/3.29.2"
          echo "module load openmpi/gcc112/psm2/4.0.5"
  else
    module load userspace
    module load cmake/3.29.2
    module load openmpi/gcc112/psm2/4.0.5 # comment if openmpi 5.
  fi
fi

H5_DIR=${HOME}/LIBRARY_PARA/LIBRARIES/hdf5-1.12.3
#H5_DIR=${HOME}/LIBRARY_PARA/LIBRARIES/ompi500/hdf5-1.14.4.3
env CXX=mpicxx \
#env CXX=${HOME}/LIBRARY_PARA/LIBRARIES/gnu/i8/openmpi-5.0.0/bin/mpicxx \
CC=${H5_DIR}/bin/h5pcc \
 cmake -DCMAKE_C_FLAGS="-g -DDEBUG -DH5_HAVE_PARALLEL -isystem ${H5_DIR} -isystem ~/LIBS/include" \
 -DHDF5_DIR=${H5_DIR} \
 ..

make ph5_dataset VERBOSE=1

echo "modifier lance.slurm mantenant pour ph5_dataset"


