# mpi_test_perf

## Requirements
on Ubuntu:
- openmpi-bin
- parallel HDF5, recompile from [HDF5 sources](https://github.com/HDFGroup/hdf5/releases), look release_docs/INSTALL_parallel

## 
based on [example ph5_dataset.c](https://github.com/HDFGroup/hdf5/blob/hdf5_1_14/HDF5Examples/C/H5PAR/ph5_dataset.c) 
```commandline
./configure --enable-parallel --prefix=$HOME/LIBRARY_PARA/LIBRARIES/hdf5-1.14.4.3
make install
```
only C interface is compiled

## test for 2^24
```
bash build.sh
mpirun -np 4 ./build/ph5_dataset 24 ph5_dataset_24_4.h5
```
et ça donne sur une machine séquentielle:
```
You have entered 3 arguments:
./build/ph5_dataset
24
ph5_dataset_24_4.h5
you choose 2^24
writeH5compressed:46 dims: 16777216 
size of chunk: 8192
PHDF5 ph5_dataset_24_4.h5 example finished with no errors

real	0m1,241s
user	0m2,856s
sys	0m0,682s
```

## mesocentre (cluster) avec with GPFS

[//]: # (### 2^22 avec 2 noeuds: 8755891 5mn)

[//]: # (### 2^24 avec 1 noeuds: 8782385 5mn 19s pour ~9Go)

[//]: # (```commandline)

[//]: # (```)

[//]: # (### 2^24 avec 2 noeuds: 8755903 toujours pas fini au bout de 10mn &#40;et consomme &#41;)

| jobid           | array size | walltime |  h5 size   |    ressources |
|:----------------|:----------:|---------:|:----------:|--------------:|
| 8822566             |    2^24    |      14s | 31Mb/268Mb | 4 nodes/3,7Mb |
| 8808340             |    2^32    |    1mn44 | ?Gb/128Gb  |  4 nodes/36Gb |
|  |            |          |            |               |

avec la version
commit c1c6028b608ea2425abb1765bb68a4bf62c52f6b
Date:   Tue Oct 1 15:38:26 2024 +0200

Test avec MPI 4.0.5 et HDF5 1.12.3

| jobid           | nb coeurs | array size   | chunk size | h5 size | walltime |
|:----------------|:---------:|:-------------|:----------:|--------------:|----------|
| 8834781 |   4*32    | 2^33         |  2^20 (1048576)          |      9,2Go         | 2mn13    |
| 8835607 |   4*32    | 2^33         | 2^19 (~500K) | 9,4gO | 2mn      |

avec la version corrigée
commit c0faf93276fbe8f5da51f8dabdf624d742e75501
Date:   Tue Oct 1 17:06:37 2024 +0200

| jobid           | nb coeurs | array size   | chunk size | h5 size | walltime |
|:----------------|:---------:|:-------------|:----------:|--------------:|----------|
| 8835638 |    4*32    | 2^33         | sans compression | |     |


avec la version de la branche arthurpiquet

| jobid           | nb coeurs | array size   | chunk size | h5 size | walltime |
|:----------------|:---------:|:-------------|:----------:|--------------:|----------|
| 8838430 |   4*32    | 2^33         | 2^19 (~500K) | 9,4gO | 1mn50      |

## athena-4to (smp)
# 
| jobid           | array size |        walltime |  h5 size   |    ressources |
|:----------------|:----------:|----------------:|:----------:|--------------:|
| 374             |    2^32    |           2mn43 | 7,9Gb/64Gb | 4 procs/138Go |
| 377             |    2^33    |            20mn | 13Gb/128Gb |  1 proc/275Gb |
|  |            |  |            |               |


```