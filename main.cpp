#include <iostream>
#include <cmath>
#include <mpi.h>     /* MPI and MPI-IO live here */

using namespace std;

extern "C" {
#include "ph5_file_utils.h"
}

typedef unsigned long long hsize_t;

int
main(int argc, char **argv) {
    s1_t *data;
    hsize_t dimsf[1]; /* dataset dimensions */
    bool compressed = true;

    /*
     * Initialize MPI
     */
    MPI_Init(&argc, &argv);
    int mpi_size, mpi_rank;

    int expo_chunk;

    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    if (mpi_rank == 0) {
        cout << "You have entered " << argc << " arguments:" << "\n";
        if (argc < 5) {
            cout << argc << endl;
            cout << "Usage: " << argv[0] << " <exposant> <fichier resultat> <compression> <chunk size>" << endl;
            cout << "exposant: n dans 2^n" << endl;
            cout << "fichier resultat: nom du fichier HDF5" << endl;
            cout << "compression: true (0) false (1)" << endl;
            cout << "chunk size: size of chunk if compressed, given by n (2^n)" << endl;
            return EXIT_FAILURE;
        }
    }
#ifdef DEBUG1
    for (int i = 0; i < argc; ++i) {
        if (mpi_rank == 0) {
            cout << argv[i] << "\n";
        }
    }
#endif

    int expo;
    string nom_fich;  // nom du fichier de sortie, par défaut voir le .h
    string is_compressed;
    if (argc == 5) {
        expo = std::atoi(argv[1]);
        nom_fich = argv[2];
        is_compressed = argv[3];
        expo_chunk = std::atoi(argv[4]);
    }

/*
 * Initialize data buffer
 */
    dimsf[0] = PowerOfTwo(expo);
    if (mpi_rank == 0) {
        cout << "problem size: 2^" << expo << "(" << dimsf[0] << ")" << endl;
        cout << "h5 result: " << nom_fich << endl;
    }
    string str_true("true");
    if (is_compressed == str_true) {
        if (mpi_rank == 0) {
            cout << "compression with chunk size 2^" << expo_chunk << "(" << PowerOfTwo(expo_chunk) << ")" << endl;
        }
    } else {
        if (mpi_rank == 0) {
            cout << "pas de compression" << endl;
        }
        expo_chunk = 0;
        compressed = false;
    }

//AP: subset of 2^n for each mpi process. Each process has n=count data.
//AP: By using "ceil", you can use any mpi_size (odd or even), so not necessarily a multiple of 2
    hsize_t count = ceil(dimsf[0] / (double) mpi_size);
    hsize_t offset = mpi_rank * count;
//AP: The last process can have less data than the other
    if (mpi_rank == mpi_size - 1)
        count = dimsf[0] - offset;

    data = (s1_t *) malloc(sizeof(s1_t) * count);
    for (hsize_t i = 0; i < count; i++) {
        data[i].a = 1.1 * (i + offset);
        data[i].b = 1.1 * (i * i + offset);
    }

    if (writeH5compressed(nom_fich.c_str(), data, dimsf, compressed, expo_chunk) == EXIT_FAILURE) {
        cout << " Error " << endl;
        return EXIT_FAILURE;
    };

//AP: don't do free() inside write function as it could lead to memory leak
    free(data);

    MPI_Finalize();

    return 0;
}

