/*
 *  This example writes data to the HDF5 file.
 *  Number of processes is assumed to be 1 or multiples of 2 (up to 8)
 */

#include "stdlib.h"
#include "math.h"
#include <errno.h>
#include "hdf5.h"

#include "ph5_file_utils.h"

int writeH5compressed(const char *str,
                      const s1_t *data,
                      const hsize_t *dimsf,
                      const bool compressed,
                      const int chunk) {
    /*
     * HDF5 APIs definitions
     */
    hid_t file_id, dset_id;   /* file and dataset identifiers */
    hid_t filespace;          /* file and memory dataspace identifiers */
    /*hsize_t dimsf[] = {NX * NY};*/ /* dataset dimensions */
    /* s1_t *data;*/               /* pointer to data buffer to write */
    hid_t plist_id;           /* property list identifier */
    herr_t status;

    hid_t s1_tid;     /* File datatype identifier */
    //AP: chunk dim are for the compression process. You must configure it by testing the perfomance on your cluster/pc.
    //AP: If you use too big chunk, it will affect performance. It performs better with multi-dimensionnal array.
    hsize_t chunk_dim = 8192;  /* 2^20 = 1M; 2^26 ~ 64Mo */

    /*
     * MPI variables
     */
    int mpi_size, mpi_rank;


    MPI_Comm comm = MPI_COMM_WORLD;
    MPI_Info info = MPI_INFO_NULL;

    MPI_Comm_size(comm, &mpi_size);
    MPI_Comm_rank(comm, &mpi_rank);

    if (compressed) {
        if (chunk > 0) {
            chunk_dim = PowerOfTwo(chunk);
            if (dimsf[0] < chunk_dim) {
                printf("Erreur chunk size %lld or full size %lld \n", chunk_dim, dimsf[0]);
                printf("chunk size < full size\n");
                return (EXIT_FAILURE);
            }
#ifdef DEBUG
            if (mpi_rank == 0) {
              printf("%s:%d dims: %lld \n", __FUNCTION__, __LINE__, (int) dimsf[0]);
            }
#endif
        }
    }

    /*
     * Set up file access property list with parallel I/O access
     */
    plist_id = H5Pcreate(H5P_FILE_ACCESS);
    if (plist_id == H5I_INVALID_HID) {
        return EXIT_FAILURE;
    }
    H5Pset_fapl_mpio(plist_id, comm, info);

    /*
     * OPTIONAL: It is generally recommended to set collective
     *           metadata reads on FAPL to perform metadata reads
     *           collectively, which usually allows datasets
     *           to perform better at scale, although it is not
     *           strictly necessary.
     */
    H5Pset_all_coll_metadata_ops(plist_id, true);

    /*
     * OPTIONAL: It is generally recommended to set collective
     *           metadata writes on FAPL to perform metadata writes
     *           collectively, which usually allows datasets
     *           to perform better at scale, although it is not
     *           strictly necessary.
     */
    H5Pset_coll_metadata_write(plist_id, true);

    /*
     * Create a new file collectively and release property list identifier.
     */
    file_id = H5Fcreate(str, H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
    H5Pclose(plist_id);

    /*
     * Create the dataspace for the dataset.
     */
    filespace = H5Screate_simple(1, dimsf, NULL);

    /*
     * Create the memory datatype.
     */
    s1_tid = H5Tcreate(H5T_COMPOUND, sizeof(s1_t));
    H5Tinsert(s1_tid, "re", HOFFSET(s1_t, a), H5T_NATIVE_DOUBLE);
    H5Tinsert(s1_tid, "im", HOFFSET(s1_t, b), H5T_NATIVE_DOUBLE);

    //compressed = false;
    if (compressed) {
        plist_id = H5Pcreate(H5P_DATASET_CREATE);
        /* Dataset must be chunked for compression */
        status = H5Pset_chunk(plist_id, 1, &chunk_dim);

        /* Set ZLIB / DEFLATE Compression using compression level 6.
         * To use SZIP Compression comment out these lines.
         */
        status = H5Pset_shuffle(plist_id);
        status = H5Pset_deflate(plist_id, 6);

        /* Uncomment these lines to set SZIP Compression
           szip_options_mask = H5_SZIP_NN_OPTION_MASK;
           szip_pixels_per_block = 16;
           status = H5Pset_szip (plist_id, szip_options_mask, szip_pixels_per_block);
        */

        dset_id = H5Dcreate2(file_id, DATASETNAME, s1_tid, filespace, H5P_DEFAULT, plist_id,
                             H5P_DEFAULT);
        H5Pclose(plist_id);
    } else {
        /*
         * Create the dataset with default properties and close filespace.
         */
        dset_id =
                H5Dcreate(file_id, DATASETNAME, s1_tid, filespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    }

    /*
     * Create property list for collective dataset write.
     */
    plist_id = H5Pcreate(H5P_DATASET_XFER);
    if (plist_id == H5I_INVALID_HID) {
        return EXIT_FAILURE;
    }
    H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);

    //AP: again, as for the data, each sub-array needs to be written (collectively) at their correct location within the big-array (2^n)
    hsize_t count = ceil(dimsf[0] / (double) mpi_size);
    hsize_t offset = mpi_rank * count;
    if (mpi_rank == mpi_size - 1)
        count = dimsf[0] - offset;

    hid_t wspace = H5Dget_space(dset_id);
    hid_t mspace = H5Screate_simple(1, &count, NULL);
    H5Sselect_hyperslab(wspace, H5S_SELECT_SET, &offset, NULL, &count, NULL);

    /*
     * To write dataset independently use
     *
     * H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_INDEPENDENT);
     */

    status = H5Dwrite(dset_id, s1_tid, mspace, wspace, plist_id, data);

    /*
     * Close/release resources.
     */
    H5Dclose(dset_id);
    H5Sclose(filespace);
    H5Pclose(plist_id);
    H5Fclose(file_id);

    if (mpi_rank == 0)
        printf("PHDF5 %s example finished with no errors\n", str);

    return 0;
}
