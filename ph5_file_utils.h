#ifndef PH5_FILE_UTILS_H
#define PH5_FILE_UTILS_H

#define H5FILE_NAME "SDScomp1d.h5"
#define DATASETNAME "DoubleArray1dNotComp"
#define NX          64 /* dataset dimensions toujours multiple de 2 dans notre cas */
#define NY          1
#define RANK        1

typedef struct s1_t {
    double a;
    double b;
} s1_t;

#include <stdbool.h>

typedef unsigned long long hsize_t;

//AP: power of two is a really simple function because we can take advantage of data written in base 2. No need for a loop.
inline unsigned long long PowerOfTwo(unsigned int number) {
    unsigned long long tmp = 1;
    return tmp << number;
}

//AP: everything should be const as this is a write function. It means that the data will not be overwritten and tell the compiler that it can optimize some part.
int writeH5compressed(const char *nomfich, const s1_t *data, const hsize_t *dimsf,
                      const bool compressed, const int chunk);

#endif //PH5_FILE_UTILS_H//

